#include <stdio.h>
int main() {
    int x, y;
    printf("Enter a positive integer: ");
    scanf("%d",&x);
    printf("Factors of %d are: ",x);
    for (y= 1; y <= x; ++y) {
        if (x % y == 0) {
            printf("%d , ",y);
        }
    }
    return 0;
}

